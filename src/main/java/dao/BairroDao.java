package dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import model.Bairro;
import model.Municipio;

public class BairroDao {

	private EntityManager em;

	public BairroDao(EntityManager em) {
		this.em = em;
	}

	public Bairro recuperaPorNome(Municipio municipio, String nomeBairro) {
		String jpql = "select b from Bairro b where b.nome = :pNome and b.municipio = :pMunicipio";

		TypedQuery<Bairro> query = em.createQuery(jpql, Bairro.class);
		query.setParameter("pNome", nomeBairro);
		query.setParameter("pMunicipio", municipio);

		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	public void insere(Bairro bairro) {
		em.persist(bairro);
		
	}
}
