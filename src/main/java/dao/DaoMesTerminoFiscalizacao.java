//package dao;
//
//import javax.persistence.EntityManager;
//
//import model.MesTerminoFiscalizacao;
//import postgres.JPAUtil;
//
//public class DaoMesTerminoFiscalizacao {
//	
//	public void persistirMesTerminoFiscalizacao(String valor) {
//
//		EntityManager em = JPAUtil.getEntityManager();
//		
//		MesTerminoFiscalizacao mesTermino = new MesTerminoFiscalizacao();
//		mesTermino.setMesTerminoFiscalizacao(valor);
//		
//		try {
//			em.getTransaction().begin();
//			
//			em.persist(mesTermino);
//
//			em.getTransaction().commit();
//
//		} catch (Exception ex) {
//			em.getTransaction().rollback();
//		}
//	}
//}
