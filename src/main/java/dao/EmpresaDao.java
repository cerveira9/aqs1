package dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import model.Empresa;

public class EmpresaDao {

	private EntityManager em;

	public EmpresaDao(EntityManager em) {
		this.em = em;
	}

	public Empresa recuperaPorCnpj(String cnpj) {
		String jpql = "select e from Empresa e where e.cnpj = :pCnpj";
		
		TypedQuery<Empresa> query = em.createQuery(jpql, Empresa.class);
		query.setParameter("pCnpj", cnpj);

		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	public void insere(Empresa empresa) {
		em.persist(empresa);
	}

}
