package dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import model.Municipio;
import model.Uf;

public class MunicipioDao {

	private EntityManager em;

	public MunicipioDao(EntityManager em) {
		this.em = em;
	}

	public Municipio recuperaPorNome(Uf uf, String nomeMunicipio) {
		String jpql = "select m from Municipio m where m.nome = :pNome and m.uf = :pUf";
		
		TypedQuery<Municipio> query = em.createQuery(jpql, Municipio.class);
		query.setParameter("pNome", nomeMunicipio);
		query.setParameter("pUf", uf);
		
		try {
			return query.getSingleResult();
		} catch(NoResultException ex) {
			return null;
		}
	}

	public void insere(Municipio municipio) {
		em.persist(municipio);
	}

}
