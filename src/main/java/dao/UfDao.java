package dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import model.Uf;

public class UfDao {

	private EntityManager em;

	public UfDao(EntityManager em) {
		this.em = em;
	}

	public void insere(Uf uf) {
		em.persist(uf);		
	}

	public Uf recuperaPorNome(String nomeUf) {
		String jpql = "select u from Uf u where u.nome = :uNome";
		
		TypedQuery<Uf> query = em.createQuery(jpql, Uf.class);
		query.setParameter("uNome", nomeUf);
		
		try {
			return query.getSingleResult();
		} catch(NoResultException ex) {
			return null;
		}
	}
	
}
