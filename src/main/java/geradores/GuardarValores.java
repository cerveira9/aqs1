package geradores;

import model.Bairro;
import model.Empresa;
import model.Fiscalizacao;
import model.Municipio;
import model.Rejeitos;
import model.Uf;

public class GuardarValores {
	
	Empresa empresa = new Empresa();
	Fiscalizacao fiscalizacao = new Fiscalizacao();
	Bairro bairro = new Bairro();
	Municipio municipio = new Municipio();
	Uf uf = new Uf();
	Rejeitos rejeitos = new Rejeitos();
	
	public GuardarValores () {
		
	}
	
	public void guardarNomeEstabelecimento (String nome) {
		this.empresa.setNomeEstabelecimento(nome);
	}
	
	public void guardarLogradouroLocalFiscalizado (String nome) {
		this.empresa.setLogradouroLocalFiscalizado(nome);
	}
	
	public void guardarCnpj (String numero) {
		this.empresa.setCnpj(numero);
	}
	
	public void guardarCep (String numero) {
		this.empresa.setCep(numero);
	}
}
