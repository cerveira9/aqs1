package geradores;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.persistence.EntityManager;

import model.FiscalizacaoCSV;

public class LerDados {

	private EntityManager em;

	public LerDados(EntityManager em) {
		this.em = em;
	}

	public void lerDadosDoCsv(String nomeArquivo) {

		try {
			FileReader arq = new FileReader(nomeArquivo);
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine();
			MigrarDados migrarDados = new MigrarDados(em);

			while (linha != null) {
				try {
					em.getTransaction().begin();
					linha = lerArq.readLine();

					FiscalizacaoCSV fiscalizacaoCSV = new FiscalizacaoCSV(linha);
					migrarDados.migrarMunicipio(fiscalizacaoCSV);
					migrarDados.migrarBairro(fiscalizacaoCSV);
					migrarDados.migrarEmpresa(fiscalizacaoCSV);
					em.getTransaction().commit();
				} catch (Exception ex) {
					em.getTransaction().rollback();
					System.out.println(ex.getMessage());
				}
			}
			lerArq.close();
		} catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
		}

	}

}
