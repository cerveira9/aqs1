package geradores;

import javax.persistence.EntityManager;

import dao.BairroDao;
import dao.EmpresaDao;
import dao.FiscalizacaoDao;
import dao.MunicipioDao;
import dao.UfDao;
import model.Bairro;
import model.Empresa;
import model.FiscalizacaoCSV;
import model.Municipio;
import model.Uf;

public class MigrarDados {
	
	private EntityManager em;
//	private MunicipioDao MunicipioDao;

	public MigrarDados(EntityManager em) {
		this.em = em;
	}
	
	public void migrarUf() {
		UfDao ufDao = new UfDao(em);
		try {
			em.getTransaction().begin();
			ufDao.insere(new Uf("SP", "SÃO PAULO"));
			ufDao.insere(new Uf("RJ", "RIO DE JANEIRO"));
			ufDao.insere(new Uf("MG", "MINAS GERAIS"));
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
		}
	}

	public void migrarMunicipio(FiscalizacaoCSV fiscalizacaoCSV) {
		UfDao ufDao = new UfDao(em);
		Uf uf = ufDao.recuperaPorNome(fiscalizacaoCSV.getUf());
		if (uf == null) {
			return ;
		}
		
		MunicipioDao municipioDao = new MunicipioDao(em);
		Municipio municipio = municipioDao.recuperaPorNome(uf, fiscalizacaoCSV.getMunicipio());
		
		if (municipio != null) {
			return ;
		}
		municipio = new Municipio();
		municipio.setUf(uf);
		municipio.setNome(fiscalizacaoCSV.getMunicipio());
		
		municipioDao.insere(municipio);
	}
	
	public void migrarBairro(FiscalizacaoCSV fiscalizacaoCSV) {
		UfDao ufDao = new UfDao(em);
		Uf uf = ufDao.recuperaPorNome(fiscalizacaoCSV.getUf());
		if (uf == null) {
			return ;
		}
		
		MunicipioDao municipioDao = new MunicipioDao(em);
		Municipio municipio = municipioDao.recuperaPorNome(uf, fiscalizacaoCSV.getMunicipio());
		
		if (municipio == null) {
			return ;
		}
		
		BairroDao bairroDao = new BairroDao(em);
		Bairro bairro = bairroDao.recuperaPorNome(municipio, fiscalizacaoCSV.getBairro());
		
		if (bairro != null) {
			return ;
		}
		
		bairro = new Bairro();
		bairro.setMunicipio(municipio);
		bairro.setNome(fiscalizacaoCSV.getBairro());
		
		bairroDao.insere(bairro);
	}
	
	public void migrarEmpresa(FiscalizacaoCSV fiscalizacaoCSV) {
		EmpresaDao empresaDao = new EmpresaDao(em);
		Empresa empresa = empresaDao.recuperaPorCnpj(fiscalizacaoCSV.getCnpj());
		
		if (empresa != null) {
			return ;
		}
		
		empresa = new Empresa();
		empresa.setCnpj(fiscalizacaoCSV.getCnpj());
		empresa.setNomeEstabelecimento(fiscalizacaoCSV.getNomeEstabelecimento());
		empresa.setLogradouroLocalFiscalizado(fiscalizacaoCSV.getLogradouro());
		empresa.setCep(fiscalizacaoCSV.getCep());
		
		empresaDao.insere(empresa);
	}
	
	public void migrarFiscalizacao(FiscalizacaoCSV fiscalizacaoCSV) {
		FiscalizacaoDao fiscalizacaoDao = new FiscalizacaoDao(em);
		Fiscalizacao fiscalizacao = fiscalizacaoDao.recuperaPorCnpj(fiscalizacaoCSV.getCnpj());
	}
}


//LerDados leitorDeDados = new LerDados();
//leitorDeDados.lerDadosDoCsv();

//String nome = "/home/cerveira/Documentos/AQS/A1 - Files/Teste.csv";
//
//try {
//	FileReader arq = new FileReader(nome);
//	BufferedReader lerArq = new BufferedReader(arq);
//	String linha = lerArq.readLine();
//
//	while (linha != null) {
//		linha = lerArq.readLine();
//
//		String[] linhaSeparada1 = linha.split(";");
//		
//		String anoTerminoFiscalizacaoValor = linhaSeparada1[0];
//		String mesTerminoFiscalizacaoValor = linhaSeparada1[1];
//		String cnpjValor = linhaSeparada1[2];
//		String nomeEstabelecimentoValor = linhaSeparada1[3];
//		String logradouroValor = linhaSeparada1[4];
//		String cepValor = linhaSeparada1[5];
//		String bairroValor = linhaSeparada1[6];
//		String municipioValor = linhaSeparada1[7];
//		String ufValor = linhaSeparada1[8];
//		
//		Empresa empresa = new Empresa();
//		Fiscalizacao fiscalizacao = new Fiscalizacao();
//		Bairro bairro = new Bairro();
//		Municipio municipio = new Municipio();
//		Uf uf = new Uf();
//		Rejeitos rejeitos = new Rejeitos();
//		ValidarCnpj validarCnpj = new ValidarCnpj();
//		
//		
//		
//		if (validarCnpj.isValido(cnpjValor) && ufValor == "São Paulo") {
//
//			empresa.setCnpj(cnpjValor);
//			
//			uf.setNome(ufValor);
//			uf.setSigla("SP");
//			
//			empresa.setCep(cepValor);
//			empresa.setLogradouroLocalFiscalizado(logradouroValor);
//			empresa.setNomeEstabelecimento(nomeEstabelecimentoValor);
//		
//			bairro.setBairro(bairroValor);
//
//			municipio.setMunicipio(municipioValor);
//			
//			fiscalizacao.setAnoTerminoFiscalizacao(anoTerminoFiscalizacaoValor);
//			fiscalizacao.setMesTerminoFiscalizacao(mesTerminoFiscalizacaoValor);
//			fiscalizacao.setNomeEstabelecimento(nomeEstabelecimentoValor);
//			fiscalizacao.setLogradouroLocalFiscalizado(logradouroValor);
//			fiscalizacao.setCep(cepValor);
//			
//			try {
//				em.getTransaction().begin();
//				em.persist(empresa);
//				em.persist(bairro);
//				em.persist(municipio);
//				em.persist(uf);
//				System.out.println("Entrou no Persist 2");
//				em.getTransaction().commit();
//			} catch (Exception ex) {
//				em.getTransaction().rollback();
//			}
//			
//		} else {
//			
//			rejeitos.setCnpj(cnpjValor);
//			rejeitos.setUf(ufValor);
//			rejeitos.setBairro(bairroValor);
//			rejeitos.setCep(cepValor);
//			rejeitos.setLogradouroLocalFiscalizado(logradouroValor);
//			rejeitos.setNomeEstabelecimento(nomeEstabelecimentoValor);
//			rejeitos.setMunicipio(municipioValor);
//			
//			try {
//				
//				em.getTransaction().begin();
//				em.persist(rejeitos.getCnpj());
//				em.persist(rejeitos.getLogradouroLocalFiscalizado());
//				em.persist(rejeitos.getCep());
//				em.persist(rejeitos.getNomeEstabelecimento());
//				em.persist(rejeitos.getBairro());
//				em.persist(rejeitos.getMunicipio());
//				em.persist(rejeitos.getUf());
//
//				System.out.println("Entrou no Persist 2");
//
//				em.getTransaction().commit();
//
//			} catch (Exception ex) {
//				em.getTransaction().rollback();
//			}
//		}
//	}
//
//	arq.close();
//} catch (IOException e) {
//	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
//}
//
//System.out.println();
//

