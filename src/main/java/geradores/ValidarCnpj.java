package geradores;

public class ValidarCnpj {

	private String cnpj;

	public String separandoCaracteresNumericos(String cnpj) {
		this.cnpj = cnpj
				.trim()
				.replace(".", "")
				.replace("/", "")
				.replace("-", "");
		
		return cnpj;
	}


	private boolean cnpjNaoValido(String cnpj) {
		return cnpj.equals("00000000000000") || cnpj.equals("11111111111111") || cnpj.equals("22222222222222")
				|| cnpj.equals("33333333333333") || cnpj.equals("44444444444444") || cnpj.equals("55555555555555")
				|| cnpj.equals("66666666666666") || cnpj.equals("77777777777777") || cnpj.equals("88888888888888")
				|| cnpj.equals("99999999999999");
	}

	private  boolean verificaOsDigitosVerificadores(String cnpj, String dig13, String dig14) {
		return ((dig13.equals(cnpj.substring(12, 13))) && (dig14.equals(cnpj.substring(13, 14))));
	}

	private String calculaPrimeiroDigitoVerificador(String cnpj) {
		int peso = 2;
		int somatorio = 0;
		
		for (int i = 11; i >= 0; i--) {
			int digito = Integer.parseInt( cnpj.substring(i, i + 1) ); 
			System.out.println(digito);
			somatorio += (digito * peso);
			peso++;
			if (peso == 10)
				peso = 2;
		}

		int restoDaDivisao = somatorio % 11;
		
		if ((restoDaDivisao == 0) || (restoDaDivisao == 1)) {
			return "0";
		} else {
			return  String.valueOf(11 - restoDaDivisao);
		}
	}

	private  String calculaSegundoDigitoVerificador(String cnpj) {
		int somatorio = 0;
		int peso = 2;
		for (int i = 12; i >= 0; i--) {
			int digito = Integer.parseInt( cnpj.substring(i, i + 1) ); 
			somatorio += (digito * peso);
			peso++;
			if (peso == 10)
				peso = 2;
		}

		int restoDaDIvisao = somatorio % 11;
		if ((restoDaDIvisao == 0) || (restoDaDIvisao == 1))
			return "0";
		else
			return  String.valueOf(11 - restoDaDIvisao);
	}

	public boolean isValido(String cnpj) {
		if (cnpj == "Sem Informação") {
			return false;
		}
		String numeroCnpj = separandoCaracteresNumericos(cnpj);
		System.out.println(numeroCnpj);
		
		if (numeroCnpj.length() != 14) {
			return false;
		}

		if (cnpjNaoValido(numeroCnpj)) {
			return false;
		}
		
		

		String dig13 = calculaPrimeiroDigitoVerificador(numeroCnpj);
		String dig14 = calculaSegundoDigitoVerificador(numeroCnpj);
		return verificaOsDigitosVerificadores(numeroCnpj, dig13, dig14);
	}

	public String getCnpj() {
		return cnpj;
	}


	public String getDigitosVerificadoresCalculados() {
		return calculaPrimeiroDigitoVerificador(cnpj) + calculaSegundoDigitoVerificador(cnpj);
	}
	
	
}
