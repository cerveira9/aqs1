package main;

import javax.persistence.EntityManager;

import geradores.LerDados;
import geradores.MigrarDados;
import postgres.JPAUtil;

public class Main {

	private static String nome = "/home/cerveira/Documentos/AQS/A1 - Files/Fiscalização - São Paulo.csv";
	private static EntityManager em = JPAUtil.getEntityManager();
	
	public static void main(String[] args) {
		MigrarDados migrarDados = new MigrarDados(em);
		migrarDados.migrarUf();
		LerDados lerDados = new LerDados(em);
		lerDados.lerDadosDoCsv(nome); 
		System.exit(0);
	}

}
