package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "tb_empresa")
public class Empresa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "cnpj", length = 18, nullable = true)
	private String cnpj;
	@Column(name = "nomeEstabelecimento", length = 50, nullable = true)
	private String nomeEstabelecimento;
	@Column(name = "logradouroLocalFiscalizado", length = 100, nullable = true)
	private String logradouroLocalFiscalizado;
	@Column(name = "cep", length = 9, nullable = true)
	private String cep;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}
	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}
	public String getLogradouroLocalFiscalizado() {
		return logradouroLocalFiscalizado;
	}
	public void setLogradouroLocalFiscalizado(String logradouroLocalFiscalizado) {
		this.logradouroLocalFiscalizado = logradouroLocalFiscalizado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((logradouroLocalFiscalizado == null) ? 0 : logradouroLocalFiscalizado.hashCode());
		result = prime * result + ((nomeEstabelecimento == null) ? 0 : nomeEstabelecimento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (id != other.id)
			return false;
		if (logradouroLocalFiscalizado == null) {
			if (other.logradouroLocalFiscalizado != null)
				return false;
		} else if (!logradouroLocalFiscalizado.equals(other.logradouroLocalFiscalizado))
			return false;
		if (nomeEstabelecimento == null) {
			if (other.nomeEstabelecimento != null)
				return false;
		} else if (!nomeEstabelecimento.equals(other.nomeEstabelecimento))
			return false;
		return true;
	}	
	
	
	
}
