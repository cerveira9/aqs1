package model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_fiscalizacao")
public class Fiscalizacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "anoTerminoFiscalizacao", nullable = false )
	private LocalDate anoTerminoFiscalizacao;
	@Column(name = "mesTerminoFiscalizacao", nullable = false)
	private LocalDate mesTerminoFiscalizacao;
	@Column(name = "nomeEstabelecimento", length = 50, nullable = true)
	private String nomeEstabelecimento;
	@Column(name = "logradouroLocalFiscalizado", length = 100, nullable = true)
	private String logradouroLocalFiscalizado;
	@Column(name = "cep", length = 9, nullable = true)
	private String cep;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public LocalDate getAnoTerminoFiscalizacao() {
		return anoTerminoFiscalizacao;
	}
	public void setAnoTerminoFiscalizacao(LocalDate anoTerminoFiscalizacaoValor) {
		this.anoTerminoFiscalizacao = anoTerminoFiscalizacaoValor;
	}
	public LocalDate getMesTerminoFiscalizacao() {
		return mesTerminoFiscalizacao;
	}
	public void setMesTerminoFiscalizacao(LocalDate mesTerminoFiscalizacao) {
		this.mesTerminoFiscalizacao = mesTerminoFiscalizacao;
	}
	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}
	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}
	public String getLogradouroLocalFiscalizado() {
		return logradouroLocalFiscalizado;
	}
	public void setLogradouroLocalFiscalizado(String logradouroLocalFiscalizado) {
		this.logradouroLocalFiscalizado = logradouroLocalFiscalizado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anoTerminoFiscalizacao == null) ? 0 : anoTerminoFiscalizacao.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((logradouroLocalFiscalizado == null) ? 0 : logradouroLocalFiscalizado.hashCode());
		result = prime * result + ((mesTerminoFiscalizacao == null) ? 0 : mesTerminoFiscalizacao.hashCode());
		result = prime * result + ((nomeEstabelecimento == null) ? 0 : nomeEstabelecimento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fiscalizacao other = (Fiscalizacao) obj;
		if (anoTerminoFiscalizacao == null) {
			if (other.anoTerminoFiscalizacao != null)
				return false;
		} else if (!anoTerminoFiscalizacao.equals(other.anoTerminoFiscalizacao))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (id != other.id)
			return false;
		if (logradouroLocalFiscalizado == null) {
			if (other.logradouroLocalFiscalizado != null)
				return false;
		} else if (!logradouroLocalFiscalizado.equals(other.logradouroLocalFiscalizado))
			return false;
		if (mesTerminoFiscalizacao == null) {
			if (other.mesTerminoFiscalizacao != null)
				return false;
		} else if (!mesTerminoFiscalizacao.equals(other.mesTerminoFiscalizacao))
			return false;
		if (nomeEstabelecimento == null) {
			if (other.nomeEstabelecimento != null)
				return false;
		} else if (!nomeEstabelecimento.equals(other.nomeEstabelecimento))
			return false;
		return true;
	}	

}
