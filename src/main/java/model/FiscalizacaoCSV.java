package model;

public class FiscalizacaoCSV {

	private String[] linhaSeparada;

	public FiscalizacaoCSV(String linha) {
		
		linhaSeparada = linha.split(";");
		
	}
	
	public String getAno() {
		return linhaSeparada[0];
	}
	
	public String getMes() {
		return linhaSeparada[1];
	}
	
	public String getCnpj() {
		return linhaSeparada[2].trim();
	}
	
	public String getNomeEstabelecimento() {
		return linhaSeparada[3].toUpperCase().trim();
	}
	
	public String getLogradouro() {
		return linhaSeparada[4].toUpperCase().trim();
	}
	
	public String getCep() {
		return linhaSeparada[5];
	}
	
	public String getBairro() {
		return linhaSeparada[6].toUpperCase().trim();
	}
	
	public String getMunicipio() {
		return linhaSeparada[7].toUpperCase().trim();
	}
	
	public String getUf() {
		return linhaSeparada[8].toUpperCase().trim();
	}
	
}
