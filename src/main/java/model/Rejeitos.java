package model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_rejeitos")
public class Rejeitos {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "cnpj", length = 18, nullable = true, unique = true)
	private String cnpj;
	@Column(name = "nomeEstabelecimento", length = 50, nullable = true, unique = false)
	private String nomeEstabelecimento;
	@Column(name = "logradouroLocalFiscalizado", length = 100, nullable = true, unique = false)
	private String logradouroLocalFiscalizado;
	@Column(name = "cep", length = 9, nullable = true, unique = false)
	private String cep;
	@Column(name = "anoTerminoFiscalizacao", length = 4, nullable = false, unique = false)
	private Date anoTerminoFiscalizacao;
	@Column(name = "mesTerminoFiscalizacao", length = 10, nullable = false, unique = false)
	private Date mesTerminoFiscalizacao;
	@Column(name = "municipio", length = 50, nullable = false, unique = false)
	private String municipio;
	@Column(name = "bairro", length = 50, nullable = false, unique = false)
	private String bairro;
	@Column(name = "uf", length = 20, nullable = false, unique = false)
	private String uf;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}
	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}
	public String getLogradouroLocalFiscalizado() {
		return logradouroLocalFiscalizado;
	}
	public void setLogradouroLocalFiscalizado(String logradouroLocalFiscalizado) {
		this.logradouroLocalFiscalizado = logradouroLocalFiscalizado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public Date getAnoTerminoFiscalizacao() {
		return anoTerminoFiscalizacao;
	}
	public void setAnoTerminoFiscalizacao(Date anoTerminoFiscalizacao) {
		this.anoTerminoFiscalizacao = anoTerminoFiscalizacao;
	}
	public Date getMesTerminoFiscalizacao() {
		return mesTerminoFiscalizacao;
	}
	public void setMesTerminoFiscalizacao(Date mesTerminoFiscalizacao) {
		this.mesTerminoFiscalizacao = mesTerminoFiscalizacao;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((anoTerminoFiscalizacao == null) ? 0 : anoTerminoFiscalizacao.hashCode());
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((logradouroLocalFiscalizado == null) ? 0 : logradouroLocalFiscalizado.hashCode());
		result = prime * result + ((mesTerminoFiscalizacao == null) ? 0 : mesTerminoFiscalizacao.hashCode());
		result = prime * result + ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result + ((nomeEstabelecimento == null) ? 0 : nomeEstabelecimento.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rejeitos other = (Rejeitos) obj;
		if (anoTerminoFiscalizacao == null) {
			if (other.anoTerminoFiscalizacao != null)
				return false;
		} else if (!anoTerminoFiscalizacao.equals(other.anoTerminoFiscalizacao))
			return false;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (id != other.id)
			return false;
		if (logradouroLocalFiscalizado == null) {
			if (other.logradouroLocalFiscalizado != null)
				return false;
		} else if (!logradouroLocalFiscalizado.equals(other.logradouroLocalFiscalizado))
			return false;
		if (mesTerminoFiscalizacao == null) {
			if (other.mesTerminoFiscalizacao != null)
				return false;
		} else if (!mesTerminoFiscalizacao.equals(other.mesTerminoFiscalizacao))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (nomeEstabelecimento == null) {
			if (other.nomeEstabelecimento != null)
				return false;
		} else if (!nomeEstabelecimento.equals(other.nomeEstabelecimento))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
